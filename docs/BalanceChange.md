

# BalanceChange

Change of balance of a currency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**old** | **String** | Balance before transaction |  [optional]
**delta** | **String** | Balance difference |  [optional]
**_new** | **String** | Balance after transaction |  [optional]
**currency** | [**Currency**](Currency.md) |  |  [optional]



