package com.gitlab.blockdaemon.ubiquity.tx.btc;

import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class FeeChangeAndSelectedOutputs {
	private final long amountForRecipient;
	private final long change;
	private final long fee;

	private final ArrayList<UnspentOutputInfo> outputsToSpend;

	public FeeChangeAndSelectedOutputs(long fee, long change, long amountForRecipient, ArrayList<UnspentOutputInfo> outputsToSpend) {
		this.fee = fee;
		this.change = change;
		this.amountForRecipient = amountForRecipient;
		this.outputsToSpend = outputsToSpend;
	}


}
