package com.gitlab.blockdaemon.ubiquity;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class TestUtil {

	public static String readJsonFile(String filePath) throws IOException {
		ClassLoader classLoader = UbiquityClientTest.class.getClassLoader();
		File file = new File(classLoader.getResource(filePath).getFile());
		if (!file.exists()) {
			throw new IllegalArgumentException("File does not exist");
		}
		return new String(Files.readAllBytes(file.toPath()));
	}
}
